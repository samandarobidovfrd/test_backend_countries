# workspace (GOPATH) configured at /go
FROM golang:1.17.6 as builder

#
RUN mkdir -p $GOPATH/src/app
WORKDIR $GOPATH/src/app

# Copy the local package files to the container's workspace.
COPY . ./

# installing depends and build
RUN export CGO_ENABLED=0 && \
    export GOOS=linux && \
    make build && \
    mv ./bin/app /

FROM alpine

COPY --from=builder app .
RUN mkdir config

ENTRYPOINT ["/app"]
