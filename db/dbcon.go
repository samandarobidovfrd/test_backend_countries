package db

import (
	"context"
	"fmt"
	"log"

	config "gitlab.com/samandarobidovfrd/test-backend-with-ci-cd-go/configs"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func NewConnection(con *config.Config) *mongo.Client {
	fmt.Println("MONGODB URL", con.MongoDBUrl)
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(con.MongoDBUrl))
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Mongodb connection established")

	return client
}
