package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Countries struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Population  int64              `validate:"required" json:"population" bson:"population"`
	Name        string             `validate:"required" json:"name" bson:"name"`
	Capital     string             `validate:"required" json:"capital" bson:"capital"`
	Region      string             `validate:"required" json:"region" bson:"region"`
	Area        int64              `validate:"required" json:"area" bson:"area"`
	Currencies  []string           `validate:"required" json:"currencies" bson:"currencies"`
	Flag        string             `validate:"required" json:"flag" bson:"flag"`
	Landlocked  bool               `json:"landlocked" bson:"landlocked"`
	Independent bool               `json:"independent" bson:"independent"`
}
