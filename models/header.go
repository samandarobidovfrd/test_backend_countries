package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Header struct {
	ID    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Title string             `validate:"required" json:"title" bson:"title"`
}
