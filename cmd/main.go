package main

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"
	config "gitlab.com/samandarobidovfrd/test-backend-with-ci-cd-go/configs"
	"gitlab.com/samandarobidovfrd/test-backend-with-ci-cd-go/controllers"
	"gitlab.com/samandarobidovfrd/test-backend-with-ci-cd-go/db"
)

func main() {
	r := gin.Default()

	r.Use(cors.Default())
	con := config.Load()
	db := db.NewConnection(&con).Database("test_countries")
	countryCollection := db.Collection("country")
	headerCollection := db.Collection("header")

	r.POST("/api/country", controllers.NewCountryController(countryCollection).CreateCountry)
	r.GET("/api/countries", controllers.NewCountryController(countryCollection).GetAllCountry)
	r.GET("/api/country/:id", controllers.NewCountryController(countryCollection).GetCountry)
	r.PUT("/api/country/:id", controllers.NewCountryController(countryCollection).UpdateCountry)
	r.DELETE("/api/country/:id", controllers.NewCountryController(countryCollection).DeleteCountry)

	r.GET("/api/header", controllers.NewHeaderController(headerCollection).GetHeader)

	log.Fatal(r.Run(fmt.Sprintf("%v", con.HTTPPort)))
}
