package configs

import (
	"log"
	"os"
	"path/filepath"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

type Config struct {
	Environment string //develop,staging,production
	ProjectName string

	HTTPPort string

	ImageBaseUrl string

	ContextTimeOutDuration int

	MongoDBUrl  string
	MONGDB_NAME string
}

func Load() Config {
	dir, err := filepath.Abs(filepath.Dir("."))
	if err != nil {
		panic(err)
	}

	if err := godotenv.Load(filepath.Join(dir, ".env")); err != nil {
		log.Print("No .env file found")
	}

	config := Config{}
	config.HTTPPort = cast.ToString(env("HTTP_PORT", ":8089"))
	config.ProjectName = cast.ToString(env("PROJECT_NAME", "test-countries-backend"))
	config.ImageBaseUrl = cast.ToString(env("ImageBaseUrl", "test-countries-backend"))

	config.Environment = cast.ToString(env("ENVIRONMENT", EnvDevelop))
	config.ContextTimeOutDuration = cast.ToInt(env("TIMEOUT_DURATION", "5"))
	config.MongoDBUrl = cast.ToString(env("MONGODB_URL", "mongodb://admin:password@mongodb-db:27017/test_countries?authSource=admin"))
	// config.MONGDB_NAME = cast.ToString(env("MONGO_DATABASE_NAME", "test_countries"))
	return config
}

func env(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
