package controllers

import (
	"context"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/samandarobidovfrd/test-backend-with-ci-cd-go/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type NewHeader struct {
	db *mongo.Collection
}

func NewHeaderController(db *mongo.Collection) *NewHeader {
	return &NewHeader{
		db: db,
	}
}

func (h NewHeader) GetHeader(c *gin.Context) {
	var header []models.Header
	cursor, err := h.db.Find(context.Background(), bson.M{})
	if err != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	err = cursor.All(context.Background(), &header)
	if err != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	c.JSON(200, gin.H{
		"data": header,
	})
}
