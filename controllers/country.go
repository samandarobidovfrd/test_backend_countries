package controllers

import (
	"context"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
	"gitlab.com/samandarobidovfrd/test-backend-with-ci-cd-go/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type NewCountry struct {
	db *mongo.Collection
}

func NewCountryController(db *mongo.Collection) *NewCountry {
	return &NewCountry{
		db: db,
	}
}

func (co NewCountry) CreateCountry(c *gin.Context) {
	var countryInfo models.Countries
	err := c.ShouldBindJSON(&countryInfo)
	if err != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": "Something went wrong while unmarshalling request body",
		})

		return
	}

	err = validator.New().Struct(&countryInfo)
	if err != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	insertedId, err := co.db.InsertOne(context.Background(), countryInfo)
	if err != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	c.JSON(201, gin.H{
		"message": "Country created",
		"id":      insertedId.InsertedID,
	})
}

func (co NewCountry) GetCountry(c *gin.Context) {
	var countryInfo models.Countries
	id := c.Param("id")
	if len(id) == 0 {
		log.Println("ID not found")

		c.JSON(400, gin.H{
			"message": "ID not found",
		})

		return
	}

	objId, _ := primitive.ObjectIDFromHex(id)

	err := co.db.FindOne(context.Background(), bson.M{"_id": objId}).Decode(&countryInfo)
	if err != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	c.JSON(200, gin.H{
		"data": countryInfo,
	})
}

func (co NewCountry) GetAllCountry(c *gin.Context) {
	var countriesInfo []models.Countries
	cursor, err := co.db.Find(context.Background(), bson.M{})
	if err != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	err = cursor.All(context.Background(), &countriesInfo)
	if err != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	c.JSON(200, gin.H{
		"data": countriesInfo,
	})
}

func (co NewCountry) DeleteCountry(c *gin.Context) {
	id := c.Param("id")
	if len(id) == 0 {
		log.Println("ID not found")

		c.JSON(400, gin.H{
			"message": "ID not found",
		})

		return
	}

	objId, _ := primitive.ObjectIDFromHex(id)

	deletedResult, err := co.db.DeleteOne(context.Background(), bson.M{"_id": objId})
	if err != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	if deletedResult.DeletedCount == 0 {
		log.Println("Something went wrong while deleting")

		c.JSON(400, gin.H{
			"message": "Not deleted",
		})

		return
	}

	c.JSON(200, gin.H{
		"message": "Deleted " + id,
	})
}

func (co NewCountry) UpdateCountry(c *gin.Context) {
	var updateCountry models.Countries
	id := c.Param("id")
	if len(id) == 0 {
		log.Println("ID not found")

		c.JSON(400, gin.H{
			"message": "ID not found",
		})

		return
	}

	err := c.ShouldBindJSON(&updateCountry)
	if err != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	err = validator.New().Struct(&updateCountry)
	if err != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	objId, _ := primitive.ObjectIDFromHex(id)

	errUpd := co.db.FindOneAndUpdate(context.Background(), bson.M{"_id": objId}, bson.M{"$set": updateCountry})
	if errUpd.Err() != nil {
		log.Println(err)

		c.JSON(400, gin.H{
			"message": err.Error(),
		})

		return
	}

	c.JSON(200, gin.H{
		"message": "Updated",
	})
}
